import com.google.common.io.Resources;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 *
 */
public class SampleRecommender
{
	public static void main(String[] args) throws IOException, TasteException
	{
		URL resource = Resources.getResource("dataset.csv");
		DataModel model = new FileDataModel(new File(resource.getFile()));
		UserSimilarity similarity = new PearsonCorrelationSimilarity(model);
		UserNeighborhood neighborhood = new ThresholdUserNeighborhood(0.1, similarity, model);
		UserBasedRecommender recommender = new GenericUserBasedRecommender(model, neighborhood, similarity);
		
		int userId = 2;
		System.out.println("рекомендации для userId = " + userId);
		List<RecommendedItem> recommendations = recommender.recommend(userId, 3);
		for (RecommendedItem recommendation : recommendations)
		{
			System.out.println(recommendation);
		}
	}
}
